<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    //Admin login
    public function adminlogin(){
        return view('admin.auth.login');
    }
}
